package com.jm.jmsql.xplora;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michael L.R. Marques on 2/17/2015.
 */
@Root
public class Databases extends Item<Item, Database> {

    private static final String FILE_DATABASES = "/settings/databases.xml";

    /**
     * @param databases
     */
    public Databases(@ElementList(name = "database", inline = true, required = false, empty = true) List<Database> databases) {
        super("Databases", databases);
    }

    /**
     * @return
     */
    @Override
    @ElementList(name = "database", inline = true, required = false, empty = true)
    public List<Database> getChildren() {
        return super.getChildren();
    }

    /**
     * @return
     */
    public List<Definition> getConnectedDefinitions() {
        List<Definition> definitions = new ArrayList();
        for (Database database : this) {
            for (Definition definition : database) {
                if (((Definition) definition).isConnected()) {
                    definitions.add(definition);
                }
            }
        }
        return definitions;
    }

    /**
     * @return
     */
    public List<Definition> getDefinitions() {
        List<Definition> definitions = new ArrayList();
        for (Database database : this) {
            for (Definition definition : database) {
                definitions.add(definition);
            }
        }
        return definitions;
    }

    /**
     * @return
     */
    public boolean save(Context context) {
        Serializer serializer = new Persister(new AnnotationStrategy());
        try {
            serializer.write(this, new File(context.getExternalFilesDir(null).getAbsolutePath() + FILE_DATABASES));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * @param
     * @return
     */
    public static Databases generate(Context context) throws Exception {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            File file = new File(context.getExternalFilesDir(null).getAbsolutePath() + FILE_DATABASES);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            if (!file.exists()) {
                Databases databases = new Databases(new ArrayList<Database>(0));
                if (!databases.save(context)) {
                    Toast.makeText(context, "Databases could not be saved", Toast.LENGTH_SHORT);
                }
                return databases;
            }

            Serializer serializer = new Persister(new AnnotationStrategy());
            return serializer.read(Databases.class, file);
        }
        return new Databases(new ArrayList<Database>(0));
    }

    /**
     * @param inputStream
     * @return
     * @throws Exception
     */
    public static Databases generate(InputStream inputStream) throws Exception {
        Serializer serializer = new Persister(new AnnotationStrategy());
        return serializer.read(Databases.class, inputStream);
    }

    /**
     * @param reader
     * @return
     * @throws Exception
     */
    public static Databases generate(Reader reader) throws Exception {
        Serializer serializer = new Persister(new AnnotationStrategy());
        return serializer.read(Databases.class, reader);
    }

}
