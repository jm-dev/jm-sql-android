package com.jm.jmsql.xplora.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jm.jmsql.R;
import com.jm.jmsql.xplora.Database;
import com.jm.jmsql.xplora.Databases;
import com.jm.jmsql.xplora.Definition;

import java.util.ArrayList;

/**
 * Created by Michael L.R. Marques on 2/17/2015.
 */
public class DatabasesAdapter extends BaseExpandableListAdapter {

    private final Context context;
    private final Databases databases;
    private final LayoutInflater inflater;

    /**
     *
     * @param context
     */
    public DatabasesAdapter(Context context) {
        this(context, new Databases(new ArrayList<Database>(0)));
    }

    /**
     *
     * @param context
     * @param databases
     */
    public DatabasesAdapter(Context context, Databases databases) {
        this.context = context;
        this.databases = databases;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     *
     * @param groupPosition
     * @param childPosition
     * @param isLastChild
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_view_navigation_item, null);

            holder = new ViewHolder();
            holder.icon = (ImageView) convertView.findViewById(R.id.icon);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            holder.expand = (ImageButton) convertView.findViewById(R.id.expand);

            Definition definition = this.databases.get(groupPosition).get(childPosition);
            holder.icon.setImageResource(definition.getIcon());
            holder.text.setText(definition.getName());
            holder.expand.setVisibility(View.INVISIBLE);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        return convertView;
    }

    /**
     *
     * @param groupPosition
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_view_navigation_item, null);

            holder = new ViewHolder();
            holder.icon = (ImageView) convertView.findViewById(R.id.icon);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            holder.expand = (ImageButton) convertView.findViewById(R.id.expand);

            Database database = this.databases.get(groupPosition);
            holder.icon.setImageResource(database.getIcon());
            holder.text.setText(database.getName());

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (isExpanded) {
            holder.expand.setImageResource(R.drawable.ic_action_collapse);
        } else {
            holder.expand.setImageResource(R.drawable.ic_action_expand);
        }

        return convertView;
    }

    /**
     *
     * @return
     */
    @Override
    public int getGroupCount() {
        return this.databases.size();
    }

    /**
     *
     * @param groupPosition
     * @return
     */
    @Override
    public int getChildrenCount(int groupPosition) {
        return this.databases.get(groupPosition).size();
    }

    /**
     *
     * @param groupPosition
     * @return
     */
    @Override
    public Object getGroup(int groupPosition) {
        return this.databases.get(groupPosition);
    }

    /**
     *
     * @param groupPosition
     * @param childPosition
     * @return
     */
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.databases.get(groupPosition).get(childPosition);
    }

    /**
     *
     * @param groupPosition
     * @return
     */
    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    /**
     *
     * @param groupPosition
     * @param childPosition
     * @return
     */
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean hasStableIds() {
        return false;
    }

    /**
     *
     * @param groupPosition
     * @param childPosition
     * @return
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    /**
     *
     */
    private static class ViewHolder {
        ImageView icon;
        TextView text;
        ImageButton expand;
    }

}
