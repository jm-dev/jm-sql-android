package com.jm.jmsql.xplora.exceptions;

import java.io.File;

/**
 * Created by Michael L.R. Marques on 2/17/2015.
 */
public class LoadDriverException extends Exception {

    private final File library;

    /**
     *
     * @param library
     */
    public LoadDriverException(File library) {
        super();
        this.library = library;
    }

    /**
     *
     * @param message
     * @param library
     */
    public LoadDriverException(String message, File library) {
        super(message);
        this.library = library;
    }

    /**
     *
     * @param message
     * @param throwable
     * @param library
     */
    public LoadDriverException(String message, Throwable throwable, File library) {
        super(message, throwable);
        this.library = library;
    }

    /**
     *
     * @param throwable
     * @param library
     */
    public LoadDriverException(Throwable throwable, File library) {
        super(throwable);
        this.library = library;
    }

    /**
     *
     * @return
     */
    public File getLibrary() {
        return this.library;
    }
}
