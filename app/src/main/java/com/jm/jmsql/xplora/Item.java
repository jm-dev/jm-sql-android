package com.jm.jmsql.xplora;

import android.support.annotation.NonNull;

import com.jm.jmsql.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Michael L.R. Marques on 2/17/2015.
 */
public class Item<P extends Item, C extends Item> implements List<C> {

    protected P parent;
    protected String name;
    protected List<C> children;

    protected boolean loaded;
    protected boolean loading;

    /**
     *
     */
    public Item() {
        this(null);
    }

    /**
     *
     * @param name
     */
    public Item(String name) {
        this(null, name);
    }

    /**
     *
     * @param parent
     * @param name
     */
    public Item(P parent, String name) {
        this(parent, name, new ArrayList<C>(0));
    }

    /**
     *
     * @param name
     * @param children
     */
    public Item(String name, List<C> children) {
        this(null, name, children);
    }

    /**
     *
     * @param parent
     * @param name
     * @param children
     */
    public Item(P parent, String name, List<C> children) {
        this.parent = parent;
        this.name = name;
        this.children = children == null ? new ArrayList<C>(0) : children;
        for (C item : this.children) {
            item.setParent(this);
        }
    }

    /**
     *
     * @return
     */
    public boolean hasParent() {
        return this.parent != null;
    }

    /**
     *
     * @param <T>
     * @param parentType
     * @return
     */
    public final <T extends Item> T getParent(Class<T> parentType) {
        if (!hasParent()) {
            return null;
        } else if (getParent().getClass().equals(parentType) ||
                getParent().getClass().getSuperclass().equals(parentType)) {
            return (T) getParent();
        } else {
            return (T) getParent().getParent(parentType);
        }
    }

    /**
     *
     * @return JMSqlObject
     */
    public final P getParent() {
        return this.parent;
    }

    /**
     *
     * @param parent
     */
    public final void setParent(P parent) {
        this.parent = parent;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public List<C> getChildren() {
        if (this.children == null) {
            this.children = new ArrayList<C>();
        }
        return children;
    }

    /**
     *
     * @param children
     */
    public void setChildren(List<C> children) {
        this.children = children;
    }

    /**
     *
     * @return
     */
    public int getIcon() {
        return R.drawable.database;
    }

    /**
     *
     * @return
     */
    public boolean isSortable() {
        return !isEmpty();
    }

    /**
     *
     * @param loaded
     */
    public final void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    /**
     *
     * @return boolean
     */
    public final boolean isLoaded() {
        return this.loaded;
    }

    /**
     * @return the loading
     */
    public boolean isLoading() {
        return loading;
    }

    /**
     * @param loading the loading to set
     */
    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    /**
     *
     * @return
     */
    public Item load() {
        setLoading(false);
        setLoaded(true);
        return this;
    }

    /**
     *
     * @param location
     * @param object
     */
    public void add(int location, C object) {
        object.setParent(this);
        getChildren().add(location, object);
    }

    /**
     *
     * @param object
     * @return
     */
    public boolean add(C object) {
        object.setParent(this);
        return getChildren().add(object);
    }

    /**
     *
     * @param location
     * @param collection
     * @return
     */
    public boolean addAll(int location, Collection<? extends C> collection) {
        return getChildren().addAll(location, collection);
    }

    /**
     *
     * @param collection
     * @return
     */
    public boolean addAll(Collection<? extends C> collection) {
        return getChildren().addAll(collection);
    }

    /**
     *
     */
    @Override
    public void clear() {
        getChildren().clear();
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean contains(Object object) {
        return getChildren().contains(object);
    }

    /**
     *
     * @param collection
     * @return
     */
    @Override
    public boolean containsAll(Collection<?> collection) {
        return getChildren().containsAll(collection);
    }

    /**
     *
     * @param location
     * @return
     */
    @Override
    public C get(int location) {
        return getChildren().get(location);
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public int indexOf(Object object) {
        return getChildren().indexOf(object);
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        return getChildren().isEmpty();
    }

    /**
     *
     * @return
     */
    @NonNull
    @Override
    public Iterator<C> iterator() {
        return getChildren().iterator();
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public int lastIndexOf(Object object) {
        return getChildren().lastIndexOf(object);
    }

    /**
     *
     * @return
     */
    @NonNull
    @Override
    public ListIterator<C> listIterator() {
        return getChildren().listIterator();
    }

    /**
     *
     * @param location
     * @return
     */
    @NonNull
    @Override
    public ListIterator<C> listIterator(int location) {
        return getChildren().listIterator(location);
    }

    /**
     *
     * @param location
     * @return
     */
    @Override
    public C remove(int location) {
        return getChildren().remove(location);
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean remove(Object object) {
        return getChildren().remove(object);
    }

    /**
     *
     * @param collection
     * @return
     */
    @Override
    public boolean removeAll(Collection<?> collection) {
        return getChildren().removeAll(collection);
    }

    /**
     *
     * @param collection
     * @return
     */
    @Override
    public boolean retainAll(Collection<?> collection) {
        return getChildren().retainAll(collection);
    }

    /**
     *
     * @param location
     * @param object
     * @return
     */
    public C set(int location, C object) {
        return getChildren().set(location, object);
    }

    /**
     *
     * @return
     */
    @Override
    public int size() {
        return getChildren().size();
    }

    /**
     *
     * @param start
     * @param end
     * @return
     */
    @NonNull
    @Override
    public List<C> subList(int start, int end) {
        return getChildren().subList(start, end);
    }

    /**
     *
     * @return
     */
    @NonNull
    @Override
    public Object[] toArray() {
        return getChildren().toArray();
    }

    /**
     *
     * @param array
     * @param <T>
     * @return
     */
    @NonNull
    @Override
    public <T> T[] toArray(T[] array) {
        return getChildren().toArray(array);
    }

}
