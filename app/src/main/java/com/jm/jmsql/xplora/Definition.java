package com.jm.jmsql.xplora;

import android.os.AsyncTask;
import android.util.Base64;

import com.jm.jmsql.R;
import com.jm.jmsql.xplora.exceptions.LoadDriverException;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.convert.Convert;
import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Michael L.R. Marques on 2/17/2015.
 */
@Root
public class Definition extends Item<Database, Item> {

    @Element(name = "datasource")
    private String datasource;
    @Element(name = "username")
    private String username;
    @Element(name = "password")
    @Convert(PasswordConverter.class)
    private String password;

    private boolean library;
    private boolean executing;
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;

    /**
     *
     */
    public Definition(@Element(name = "name") String name,
                        @Element(name = "datasource") String datasource,
                            @Element(name = "username") String username,
                                @Element(name = "password") @Convert(PasswordConverter.class) String password) {
        super(name);
        this.datasource = datasource;
        this.username = username;
        this.password = password;
    }

    /**
     *
     * @return
     */
    @Override
    @Element(name = "name")
    public String getName() {
        return super.getName();
    }

    /**
     *
     * @return
     */
    @Override
    public int getIcon() {
        return isConnected() ? R.drawable.connected_definition :
                                    R.drawable.disconnected_definition;
    }

    /**
     *
     * @return
     */
    public String getDatasource() {
        return datasource;
    }

    /**
     *
     * @param datasource
     */
    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        return isConnected() &&
                super.isEmpty();
    }

    /**
     *
     * @return
     * @throws LoadDriverException
     */
    public boolean connect() throws LoadDriverException {
        try {
            connection = getParent().connect(getDatasource(), getUsername(), getPassword());
            if (connection != null ||
                    !connection.isClosed()) {
            } else {
                return false;
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     *
     */
    public void asyncConnect() {
        AsyncTask<Void, Void, Boolean> asyncTask = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    return connect();
                } catch (LoadDriverException lde) {
                    lde.printStackTrace();
                }
                return false;
            }
        };
        asyncTask.execute();
    }

    /**
     *
     * @return
     */
    public boolean disconnect() {
        try {
            if (this.statement != null) {
                this.statement.close();
                this.statement = null;
            }
            this.connection.close();
            return true;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            return false;
        }
    }

    /**
     *
     * @return
     */
    public boolean isConnected() {
        if (this.connection == null) {
            return false;
        }
        try {
            return !this.connection.isClosed();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            return false;
        }
    }

    /**
     *
     * @return
     */
    public Connection getConnection() {
        return this.connection;
    }

    /**
     *
     * @return
     */
    public DatabaseMetaData getDatabaseMetaData() {
        try {
            return this.connection.getMetaData();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param sql
     * @return
     * @throws SQLException
     */
    public boolean execute(String sql) throws SQLException {
        return execute(sql, 0);
    }

    /**
     *
     * @param limit
     * @return
     * @param sql
     * @throws java.sql.SQLException
     */
    public boolean execute(String sql, int limit) throws SQLException {
        this.executing = false;
        try {
            try {
                this.statement = this.connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            } catch (SQLException sqle) {
                this.statement = this.connection.createStatement();
                sqle.printStackTrace();
            }
            this.statement.setMaxRows(limit);
            boolean executed = this.statement.execute(sql);
            if (executed) {
                this.resultSet = this.statement.getResultSet();
            }
            return executed;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            throw sqle;
        } finally {
            this.executing = false;
        }
    }

    /**
     *
     * @throws SQLException
     */
    public void cancelExecute() throws SQLException {
        if (this.statement == null) {
            return;
        }
        try {
            this.statement.cancel();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            throw sqle;
        }
    }

    /**
     *
     * @return
     */
    public ResultSet getResults() {
        return this.resultSet;
    }

    /**
     *
     * @return
     */
    public int getRowCount() {
        if (this.resultSet == null) {
            return 0;
        }
        try {
            this.resultSet.last();
            try {
                return this.resultSet.getRow();
            } finally {
                getResults().beforeFirst();
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
        try {
            int count = 0;
            try {
                while (this.resultSet.next()) {
                    count++;
                }
            } finally {
                this.resultSet.beforeFirst();
            }
            return count;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
        try {
            return this.resultSet.getFetchSize();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
        return 0;
    }

    /**
     *
     * @return
     */
    public int getUpdateCount() {
        try {
            return this.statement.getUpdateCount();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            return 0;
        }
    }

}

/**
 *
 */
class PasswordConverter implements Converter<String> {

    private static final String ENCODING = "UTF-8";

    /**
     *
     * @param node
     * @return
     * @throws Exception
     */
    @Override
    public String read(InputNode node) throws Exception {
        String encryptedPassword = node.getValue();
        if (encryptedPassword == null ||
                encryptedPassword.isEmpty()) {
            return "";
        }

        byte[] bytes = Base64.decode(encryptedPassword, Base64.DEFAULT);
        return new String(bytes, ENCODING);
    }

    /**
     *
     * @param node
     * @param value
     * @throws Exception
     */
    @Override
    public void write(OutputNode node, String value) throws Exception {
        byte[] bytes = value.getBytes(ENCODING);
        node.setValue(new String(Base64.decode(bytes, Base64.DEFAULT)));
    }

}