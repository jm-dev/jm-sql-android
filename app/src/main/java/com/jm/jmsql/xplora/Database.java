package com.jm.jmsql.xplora;

import com.jm.jmsql.xplora.exceptions.LoadDriverException;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.convert.Convert;
import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

/**
 * Created by Michael L.R. Marques on 2/17/2015.
 */
@Root
public class Database extends Item<Databases, Definition> implements Driver {

    public static final String DEFAULT_LIBRARY_DELIMETER = ".";

    private Driver driver;
    @Element(name = "class")
    private String className;
    @Element(name = "library")
    @Convert(FileConverter.class)
    private File library;
    @Attribute(name = "library-delimeter")
    private String libraryDelimeter;
    @Attribute(name = "uppercase-preferred")
    private boolean uppercasePreferred;

    /**
     *
     * @param name
     * @param definitions
     * @param className
     * @param library
     * @param libraryDelimeter
     * @param uppercasePreferred
     */
    public Database(@Element(name = "name")String name,
                        @ElementList(name = "definition", inline = true, required = false, empty = true) List<Definition> definitions,
                            @Element(name = "class") String className,
                                @Element(name = "library") File library,
                                    @Attribute(name = "library-delimeter")String libraryDelimeter,
                                        @Attribute(name = "uppercase-preferred") boolean uppercasePreferred) {
        super(name, definitions);
        this.className = className;
        this.library = library;
        this.libraryDelimeter = libraryDelimeter;
        this.uppercasePreferred = uppercasePreferred;
    }

    /**
     *
     * @return
     */
    @Override
    @Element(name = "name")
    public String getName() {
        return super.getName();
    }

    /**
     *
     * @return
     */
    @Override
    @ElementList(name = "definition", inline = true, required = false, empty = true)
    public List<Definition> getChildren() {
        return super.getChildren();
    }

    /**
     *
     * @return
     */
    public String getClassName() {
        return className;
    }

    /**
     *
     * @param className
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     *
     * @return
     */
    public File getLibrary() {
        return this.library;
    }

    /**
     *
     * @param library
     */
    public void setLibrary(File library) {
        this.library = library;
    }

    /**
     *
     * @return
     */
    public String getLibraryDelimeter() {
        return this.libraryDelimeter;
    }

    /**
     *
     * @param libraryDelimeter
     */
    public void setLibraryDelimeter(String libraryDelimeter) {
        this.libraryDelimeter = libraryDelimeter;
    }

    /**
     *
     * @return
     */
    public boolean isUppercasePreferred() {
        return this.uppercasePreferred;
    }

    /**
     *
     * @param uppercasePreferred
     */
    public void setUppercasePreferred(boolean uppercasePreferred) {
        this.uppercasePreferred = uppercasePreferred;
    }

    /**
     *
     * @return Driver
     * @throws LoadDriverException
     */
    public Driver getDriver() throws LoadDriverException {
        if (isSystemDatabase() &&
                this.driver == null) {
            try {
                this.driver = (Driver) Class.forName(this.className).newInstance();
            } catch (ClassNotFoundException cnfe) {
                cnfe.printStackTrace();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        if (isExternalDatabase()) {
            if (!isDriverLoaded() &&
                    !isLibraryAvailable()) {
                throw new LoadDriverException(this.library.getAbsolutePath() + " cannot be found", this.library);
            } else {
                try {
                    this.driver = (Driver) new URLClassLoader(new URL[] { new URL("jar:file:" + this.library.getAbsolutePath() + "!/")} ).loadClass(this.className).newInstance();
                } catch (MalformedURLException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                    e.printStackTrace();
                }
            }
        }

        return this.driver;
    }

    /**
     *
     * @return
     */
    public boolean isDriverLoaded() {
        return isSystemDatabase() ||
                (isExternalDatabase() &&
                        this.driver != null);
    }

    /**
     *
     * @return
     */
    public boolean isLibraryAvailable() {
        return isExternalDatabase() &&
                this.library.exists();
    }

    /**
     *
     * @return
     */
    public boolean isSystemDatabase() {
        return this.library == null;
    }

    /**
     *
     * @return
     */
    public boolean isExternalDatabase() {
        return this.library != null;
    }

    /**
     *
     * @param url
     * @param username
     * @param password
     * @return
     * @throws java.sql.SQLException
     * @throws LoadDriverException
     */
    public Connection connect(String url, String username, String password) throws SQLException, LoadDriverException {
        Properties properties = new Properties();
        properties.put("user", username);
        properties.put("password", password);
        return connect(url, properties);
    }

    /**
     *
     * @param url
     * @param info
     * @return
     * @throws SQLException
     */
    @Override
    public Connection connect(String url, Properties info) throws SQLException {
        try {
            return getDriver().connect(url, info);
        } catch (LoadDriverException lde) {
            throw new SQLException("Could not load driver", lde);
        }
    }

    /**
     *
     * @param url
     * @return
     * @throws SQLException
     */
    @Override
    public boolean acceptsURL(String url) throws SQLException {
        try {
            return getDriver().acceptsURL(url);
        } catch (LoadDriverException lde) {
            throw new SQLException("Could not load driver", lde);
        }
    }

    /**
     *
     * @param url
     * @param info
     * @return
     * @throws SQLException
     */
    @Override
    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
        try {
            return getDriver().getPropertyInfo(url, info);
        } catch (LoadDriverException lde) {
            throw new SQLException("Could not load driver", lde);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public int getMajorVersion() {
        try {
            return getDriver().getMajorVersion();
        } catch (LoadDriverException lde) {
            return 0;
        }
    }

    /**
     *
     * @return
     */
    @Override
    public int getMinorVersion() {
        try {
            return getDriver().getMinorVersion();
        } catch (LoadDriverException lde) {
            return 0;
        }
    }

    /**
     *
     * @return
     */
    @Override
    public boolean jdbcCompliant() {
        try {
            return getDriver().jdbcCompliant();
        } catch (LoadDriverException lde) {
            return false;
        }
    }

}

/**
 *
 */
class FileConverter implements Converter<File> {

    /**
     *
     * @param node
     * @return
     * @throws Exception
     */
    @Override
    public File read(InputNode node) throws Exception {
        return new File(node.getValue());
    }

    /**
     *
     * @param node
     * @param value
     * @throws Exception
     */
    @Override
    public void write(OutputNode node, File value) throws Exception {
        node.setValue(value == null ? "" : value.getAbsolutePath());
    }

}
