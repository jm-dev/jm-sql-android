package com.jm.jmsql;

import android.app.Application;
import android.os.Environment;
import android.test.ApplicationTestCase;

import com.jm.jmsql.xplora.Database;
import com.jm.jmsql.xplora.Databases;
import com.jm.jmsql.xplora.Definition;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {

    public ApplicationTest() {
        super(Application.class);
    }

    public void testSavingDatabases() throws Exception {
        Databases databases = new Databases(null);
        databases.add(new Database("Test DB 1", new ArrayList<Definition>(0), Object.class.getName(), new File("."), Database.DEFAULT_LIBRARY_DELIMETER, false));

        List<Definition> definitions2 = new ArrayList<>(1);
        definitions2.add(new Definition("Def 1", "jdbc:datasource:db:0000/", "username", "password"));
        databases.add(new Database("Test DB 2", definitions2, Object.class.getName(), new File("."), Database.DEFAULT_LIBRARY_DELIMETER, true));

        databases.add(new Database("Test DB 3", new ArrayList<Definition>(0), Object.class.getName(), new File("."), Database.DEFAULT_LIBRARY_DELIMETER, false));
        databases.add(new Database("Test DB 4", new ArrayList<Definition>(0), Object.class.getName(), new File("."), Database.DEFAULT_LIBRARY_DELIMETER, true));

        boolean saved = databases.save(getContext());

        assertTrue(saved);
    }

    public void testLoadingDatabases() throws Exception {
        Databases databases = Databases.generate(getContext());

        assertNotNull(databases);
    }

}